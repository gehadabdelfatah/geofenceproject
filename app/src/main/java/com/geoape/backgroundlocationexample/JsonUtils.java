package com.geoape.backgroundlocationexample;

import android.content.Context;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pc1 on 12/03/2018.
 */

public class JsonUtils {

    //normal pattern for json object like  name:{mainName:Ham and cheese sandwich,alsoKnownAs:[]}
    private static final Pattern Normal_PATTERN =
            Pattern.compile("\"([^\"]*?)\":\"(([^\"]|(\\\\\"))*[^\\\\]{1}?)\",");
    //array pattern like alsoKnownAs:[]
    private static final Pattern ARRAY_PATTERN =
            Pattern.compile("\"([^\"]*?)\":\\[([^\\]]*?)\\]");
    private static final int NAME_POSITION = 1;
    private static final int VALUE_POSITION = 2;



    /**
     * Parses the json-string into a keyValueList by using regex.
     *
     * @param json            the json-data of the sandwiches
     * @param isSinglePattern TRUE if only single-value variables should be returned.
     *                        FALSE if only array-value variables should be returned.
     * @return the parsed json as a keyValueList.
     */
    private static HashMap<String, String> parseJsonFor(String json, boolean isSinglePattern) {
        Pattern pattern = isSinglePattern ? Normal_PATTERN : ARRAY_PATTERN;
        HashMap<String, String> parsedJson = new HashMap<>();

        Matcher m = pattern.matcher(json);
        while (m.find()) {
            String keyName = m.group(NAME_POSITION);
            String value = m.group(VALUE_POSITION);

            // if the key has an empty value then remove the value.
            if (value.equals("")) {
                value = null;
            }

            parsedJson.put(keyName, value);
        }

        return parsedJson;
    }

    /**
     * Convert a string containing commas into a List-object.
     *
     * @param value the string containing a list separated by commas.
     * @return the list as a List-object.
     */
    private static List<String> parseToArray(String value) {
        return value == null ? null : Arrays.asList(value.split(","));
    }


    public static String loadJSONFromAsset(Context context) throws IOException {
        InputStream is = context.getResources().openRawResource(R.raw.products);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            is.close();
        }
        return writer.toString();
    }
}


