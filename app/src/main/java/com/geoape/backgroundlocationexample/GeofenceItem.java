package com.geoape.backgroundlocationexample;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeofenceItem implements Parcelable
{

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("id")
    @Expose
    private String id;
    public final static Parcelable.Creator<GeofenceItem> CREATOR = new Creator<GeofenceItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GeofenceItem createFromParcel(Parcel in) {
            return new GeofenceItem(in);
        }

        public GeofenceItem[] newArray(int size) {
            return (new GeofenceItem[size]);
        }

    }
            ;

    protected GeofenceItem(Parcel in) {
        this.latitude = ((String) in.readValue((String.class.getClassLoader())));
        this.longitude = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public GeofenceItem() {
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(latitude);
        dest.writeValue(longitude);
        dest.writeValue(id);
    }

    public int describeContents() {
        return 0;
    }

}