package com.geoape.backgroundlocationexample;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class geofenceBody implements Parcelable {

    @SerializedName("geofences")
    @Expose
    private List<GeofenceItem> geofences = null;
    public final static Creator<geofenceBody> CREATOR = new Creator<geofenceBody>() {


        @SuppressWarnings({
                "unchecked"
        })
        public geofenceBody createFromParcel(Parcel in) {
            return new geofenceBody(in);
        }

        public geofenceBody[] newArray(int size) {
            return (new geofenceBody[size]);
        }

    };

    protected geofenceBody(Parcel in) {
        in.readList(this.geofences, (GeofenceItem.class.getClassLoader()));
    }

    public geofenceBody() {
    }

    public List<GeofenceItem> getProducts() {
        return geofences;
    }

    public void setProducts(List<GeofenceItem> products) {
        this.geofences = products;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(geofences);
    }

    public int describeContents() {
        return 0;
    }

}
